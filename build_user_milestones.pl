#!/usr/bin/perl -w
use strict;
use LWP::Simple;
use POSIX qw(strftime);
use DBI;
my @DSN = ("DBI:Pg:service=ddtp", "", "");

my $dbh = DBI->connect(@DSN,
    { PrintError => 0,
      RaiseError => 1,
      AutoCommit => 0,
    });

die $DBI::errstr unless $dbh;

my $sth;

# build user milestones

$sth = $dbh->prepare("DELETE FROM description_milestone_tb WHERE milestone like 'user:%' or milestone like 'lang:%'");
$sth->execute();

$sth = $dbh->prepare("INSERT INTO description_milestone_tb (description_id,milestone) VALUES ('999','user:jjjj')");
$sth->execute();

$sth = $dbh->prepare("SELECT collection,name,nametype FROM collection_milestone_tb");
$sth->execute();
while(my ($collection,$name,$nametype) = $sth->fetchrow_array) {
	my ($collectiontype,$collectionname) = ($collection =~ m/([^:]+):(.*)/);
	#print "      name    : $name\n";
	#print "      nametype: $nametype\n";
	#print "collectiontype: $collectiontype\n";
	#print "collectionname: $collectionname\n";

	my $milestone;
	my $sql;

	if ($nametype == 1) {
		$milestone="user:$name";
	} else {
		$milestone="lang:$name";
	}

	if ($collectiontype eq "mile") {
		print "-->found mile -> $milestone\n";
		$sql="INSERT INTO description_milestone_tb (description_id,milestone) SELECT DISTINCT description_id,'$milestone' FROM description_milestone_tb WHERE milestone='$collectionname' and description_id not in (SELECT DISTINCT description_id FROM description_milestone_tb WHERE milestone='$milestone')";
	} elsif ( $collectiontype eq "pkg" ) {
		print "-->found pkg -> $milestone\n";
		$sql="INSERT INTO description_milestone_tb (description_id,milestone) SELECT DISTINCT description_id,'$milestone' FROM package_version_tb WHERE package='$collectionname' and description_id not in (SELECT DISTINCT description_id FROM description_milestone_tb WHERE milestone='$milestone')";
	} elsif ( $collectiontype eq "source" ) {
		print "-->found source -> $milestone\n";
		$sql="INSERT INTO description_milestone_tb (description_id,milestone) SELECT DISTINCT description_id,'$milestone' FROM package_version_tb WHERE package in (SELECT DISTINCT package FROM description_tb WHERE source='$collectionname') and description_id not in (SELECT DISTINCT description_id FROM description_milestone_tb WHERE milestone='$milestone')";
	} elsif ( $collectiontype eq "d_id" ) {
		print "-->found d_id -> $milestone\n";
		$sql="INSERT INTO description_milestone_tb (description_id,milestone) SELECT DISTINCT description_id,'$milestone' FROM description_tb WHERE description_id='$collectionname' and description_id not in (SELECT DISTINCT description_id FROM description_milestone_tb WHERE milestone='$milestone')";
	} else {
		print "-->found unknown '$collectiontype'\n";
	}

	eval {
		#print "---->> $sql\n";
		my $sth2 = $dbh->prepare($sql);
		$sth2->execute();
	};
	if ($@) {
		warn "build_user_milestones.pl: failed to INSERT in description_milestone_tb: $@\n";
		$dbh->rollback; # undo the incomplete changes
		}

	#print "\n";
}
$dbh->disconnect();

