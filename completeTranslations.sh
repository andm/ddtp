#!/bin/bash -e

. "${DDTP_CONFIG_FILE}"

cd "${DDTP_BASE_DIR}"

# Fetch active langs from database
LANGS=`psql "${DDTP_PSQL_CONNECTION_STRING}" -q -A -t -c "select distinct language from translation_tb where description_id>1"`

for lang in $LANGS
do
        DATE=`date`
	echo -n "$DATE $lang: "
	./completeTranslations.pl $lang 
done
