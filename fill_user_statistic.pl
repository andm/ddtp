#!/usr/bin/perl -w
use strict;
use LWP::Simple;
use POSIX qw(strftime);
use DBI;
my @DSN = ("DBI:Pg:service=ddtp", "", "");

my $dbh = DBI->connect(@DSN,
    { PrintError => 0,
      RaiseError => 1,
      AutoCommit => 0,
    });

die $DBI::errstr unless $dbh;

my $sth;

$sth = $dbh->prepare("DELETE FROM statistic_tb WHERE stat like 'lang:pendingreview-%' or stat like 'lang:pendingtranslation-%'");
$sth->execute();

# lang pending

my @lang;

$sth = $dbh->prepare("select distinct language from translation_tb where description_id>1");
$sth->execute();
while(my ($lang) = $sth->fetchrow_array) {
	push (@lang,$lang);
}

foreach my $lang (@lang) {
	$sth = $dbh->prepare("SELECT count(*),CURRENT_DATE from ddtss where value='forreview' and key like ?");
	$sth->execute("$lang%");
	while(my ($count,$date) = $sth->fetchrow_array) {
		eval {
			$dbh->do("INSERT INTO statistic_tb (value,date,stat) VALUES (?,?,?);", undef, $count, $date, "lang:pendingreview-$lang");
			$dbh->commit;   # commit the changes if we get this far
		};
		if ($@) {
			warn "fill_statistic.pl: failed to INSERT stat 'lang:pendingreview-$lang': $@\n";
			$dbh->rollback; # undo the incomplete changes
		}
	}

	$sth = $dbh->prepare("SELECT count(*),CURRENT_DATE from ddtss where value like 'untranslated,%' and key like ?");
	$sth->execute("$lang%");
	while(my ($count,$date) = $sth->fetchrow_array) {
		eval {
			$dbh->do("INSERT INTO statistic_tb (value,date,stat) VALUES (?,?,?);", undef, $count, $date, "lang:pendingtranslation-$lang");
			$dbh->commit;   # commit the changes if we get this far
		};
		if ($@) {
			warn "fill_statistic.pl: failed to INSERT stat 'lang:pendingtranslation-$lang': $@\n";
			$dbh->rollback; # undo the incomplete changes
		}

	}

}
