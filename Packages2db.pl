#!/usr/bin/perl
use lib '/srv/ddtp.debian.org/ddts/bin/';
use diagnostics;
use strict;
use ddts_common;
use open ':encoding(UTF-8)';

my $packagefile= shift(@ARGV);
my $distribution= shift(@ARGV);
my $description_en_file= shift(@ARGV);   # Optional

my $description_id;
my $description_tag_id;

use DBI;
use Digest::MD5 qw(md5_hex);

my @DSN = ("DBI:Pg:service=ddtp", "", "");

my $dbh = DBI->connect(@DSN,
    { PrintError => 0,
      RaiseError => 1,
      AutoCommit => 0,
    });

die $DBI::errstr unless $dbh;

sub desc_to_parts ($) {
	my $desc = shift;

	my @parts;
	my $part;
	my @lines=split(/\n/, $desc);

	foreach (@lines) {
		if (not @parts) {
			push @parts,$_;
			$part="";
			next;
		}
		if ($_ ne " .") {
			$part.=$_;
			$part.="\n";
		} else {
			push @parts,$part if ($part ne "");
			$part="";
		}
	}
	push @parts,$part if ($part ne "");

	return @parts;

}

sub scan_packages_file {
	my $filename= shift(@_);
	my $distribution= shift(@_);
	my $description_en= shift(@_);

	my $package;
	my $prioritize;
	my $description;

	my $descr_md5;
	my $tag;
	my $source;
	my $priority;
	my $section;
	my @task;
	my @tag;
	my $version;

	sub get_old_description_id {
		my $package= shift(@_);

		my $old_description_id;

		#my $sth = $dbh->prepare("SELECT description_id FROM description_tb WHERE package=?");
		#my $sth = $dbh->prepare("SELECT description_id from package_version_tb where package=?");
		# better without LIMIT ?
		my $sth = $dbh->prepare("SELECT description_id from package_version_tb where package=? order by description_id DESC LIMIT 1");
		$sth->execute($package);
		($old_description_id) = $sth->fetchrow_array;
		return $old_description_id;
	}

	sub get_description_id_from_md5 {
		my $description_md5= shift(@_);
		my $description_id;

		my $sth = $dbh->prepare("SELECT description_id FROM description_tb WHERE description_md5=?");
		$sth->execute($description_md5);
		($description_id) = $sth->fetchrow_array;
		return $description_id;
	}

	sub get_description_from_md5 {
		my $description_md5= shift(@_);
		my $description;

		my $sth = $dbh->prepare("SELECT description FROM description_tb WHERE description_md5=?");
		$sth->execute($description_md5);
		($description) = $sth->fetchrow_array;
		return $description;
	}

	sub get_description_tag_id {
		my $description_id= shift(@_);
		my $distribution= shift(@_);

		my $description_tag_id;

		my $sth = $dbh->prepare("SELECT description_tag_id FROM description_tag_tb WHERE description_id=? and tag=?");
		$sth->execute($description_id, $distribution);
		($description_tag_id) = $sth->fetchrow_array;
		return $description_tag_id;
	}

	sub is_description_id_active {
		my $description_id= shift(@_);

		my $sth = $dbh->prepare("SELECT description_id FROM active_tb WHERE description_id=?");
		$sth->execute($description_id);
		if ($sth->fetchrow_array) {
			return 1;
		}
		return 0;
	}

	sub save_active_to_db {
		my $description_id= shift(@_);

		eval {
			$dbh->do("INSERT INTO active_tb (description_id) VALUES (?);", undef, $description_id);
			$dbh->commit;   # commit the changes if we get this far
		};
		if ($@) {
			warn "Packages2db.pl: failed to INSERT description_id '$description_id' into active_tb: $@\n";
			$dbh->rollback; # undo the incomplete changes
		}
	}

	sub save_version_to_db {
		my $description_id= shift(@_);
		my $version= shift(@_);
		my $package= shift(@_);
		my $source= shift(@_);

		my $package_version_id;

		my $sth = $dbh->prepare("SELECT package_version_id FROM package_version_tb WHERE description_id=? and package=? and version=?");
		$sth->execute($description_id, $package, $version);
		($package_version_id) = $sth->fetchrow_array;

		if (not $package_version_id) {
			eval {
				$dbh->do("INSERT INTO package_version_tb (description_id,package,version,source) VALUES (?,?,?,?);", undef, $description_id, $package, $version, $source);
				$dbh->commit;   # commit the changes if we get this far
			};
			if ($@) {
				warn "Packages2db.pl: failed to INSERT Package '$package', Version '$version' into package_version_tb: $@\n";
				$dbh->rollback; # undo the incomplete changes
			}
		}
	}

	sub save_milestone_to_db {
		my $description_id= shift(@_);
		my $priority= shift(@_);
		my $section= shift(@_);
		my @task= @_;

		my $sth = $dbh->prepare("DELETE FROM description_milestone_tb WHERE description_id=?  and 
                                                                                    not milestone like 'rtrn:%' and 
                                                                                    not milestone like 'popc:%' and 
                                                                                    not milestone like 'part:%'");
		$sth->execute($description_id);

		eval {
			$dbh->do("INSERT INTO description_milestone_tb (description_id,milestone) VALUES (?,?);", undef, $description_id, "prio:$priority");
			$dbh->commit;   # commit the changes if we get this far
		};
		if ($@) {
			warn "Packages2db.pl: failed to INSERT Package '$description_id', milestone 'prio:$priority' into description_milestone_tb: $@\n";
			$dbh->rollback; # undo the incomplete changes
		}

		eval {
			$dbh->do("INSERT INTO description_milestone_tb (description_id,milestone) VALUES (?,?);", undef, $description_id, "sect:$section");
			$dbh->commit;   # commit the changes if we get this far
		};
		if ($@) {
			warn "Packages2db.pl: failed to INSERT Package '$description_id', milestone 'sect:$section' into description_milestone_tb: $@\n";
			$dbh->rollback; # undo the incomplete changes
		}

		foreach (@task) {
			eval {
				$dbh->do("INSERT INTO description_milestone_tb (description_id,milestone) VALUES (?,?);", undef, $description_id, "task:$_");
				$dbh->commit;   # commit the changes if we get this far
			};
			if ($@) {
				warn "Packages2db.pl: failed to INSERT Package '$description_id', milestone 'task:$_' into description_milestone_tb: $@\n";
				$dbh->rollback; # undo the incomplete changes
			}
		}
	}

	sub save_tag_milestone_to_db {
		my $description_id= shift(@_);
		my @tags= @_;

		my @splittags;

                foreach my $in (@tags) {
			#print "  $description_id task: $in\n";
	                if ($in =~ m/^([^{]*)\{([^}]*)\}(.*)$/) {
		                my $prefix  = $1;
		                my $postfix = $3;
		                my @list = split /,/,$2;
		                foreach my $o (@list) {
			                push @splittags, $prefix.$o.$postfix;
			                }
	                } else {
		                push @splittags, $in;
	                }
                }

		#print "$description_id task: " . join(" ", @splittags) . "\n";

		foreach (@splittags) {
			eval {
				$dbh->do("INSERT INTO description_milestone_tb (description_id,milestone) VALUES (?,?);", undef, $description_id, "tags:$_");
				$dbh->commit;   # commit the changes if we get this far
			};
			if ($@) {
				warn "Packages2db.pl: failed to INSERT Package '$description_id', milestone 'tags:$_' into description_milestone_tb: $@\n";
				$dbh->rollback; # undo the incomplete changes
			}
		}
	}

	sub save_oldtranslang_milestone_to_db {
		my $description_id= shift(@_);
		my @oldtranslangs = @_;

		print "     save_oldtranslang_milestone_to_db: $description_id task: " . join(" ", @oldtranslangs) . " #:$#oldtranslangs\n";

		foreach (@oldtranslangs) {
			eval {
				$dbh->do("INSERT INTO description_milestone_tb (description_id,milestone) VALUES (?,?);", undef, $description_id, "rtrn:$_");
				$dbh->commit;   # commit the changes if we get this far
			};
			if ($@) {
				warn "Packages2db.pl: failed to INSERT Package '$description_id', milestone 'oldtranslangs:$_' into description_milestone_tb: $@\n";
				$dbh->rollback; # undo the incomplete changes
			}
		}
	}

	sub save_part_description_to_db {
		my $description_id= shift(@_);
		my $part_md5      = shift(@_);

		eval {
			$dbh->do("INSERT INTO part_description_tb (description_id,part_md5) VALUES (?,?);", undef, $description_id,$part_md5);
			$dbh->commit;   # commit the changes if we get this far
		};
		if ($@) {
			warn "Packages2db.pl: failed to INSERT description_id '$description_id', part_md5 '$part_md5' into part_description_tb: $@\n";
			$dbh->rollback; # undo the incomplete changes
		}
	}

	open (PACKAGES, "$filename") or die "open packagefile failed";
	my $in_descr = 0;
	while (<PACKAGES>) {
		if ($_=~/^$/) {
			my $description_orig=$description;
			my @oldtranslang;
			undef $description_id;
			eval {
				if (!defined($descr_md5)) {
					$descr_md5 = get_md5_hex($description_orig);
				}
				$description_id=get_description_id_from_md5($descr_md5);
				if ($description_id) {
					$description_tag_id=get_description_tag_id($description_id,$distribution);
					if ($description_tag_id) {
						$dbh->do("UPDATE description_tag_tb SET date_end = CURRENT_DATE WHERE description_tag_id=? AND date_end <> CURRENT_DATE;", undef, $description_tag_id);
					} else {
						$dbh->do("INSERT INTO description_tag_tb (description_id, tag, date_begin, date_end) VALUES (?,?,CURRENT_DATE,CURRENT_DATE);", undef, $description_id, $distribution);
					}

					# Fix previosly broken descriptions by a bug in import code
					my $description_in_db;
					$description_in_db = get_description_from_md5($descr_md5);
					if ($description_in_db ne $description) {
						print "Update description for $package ($description_id, $descr_md5)\n";
						$dbh->do("UPDATE description_tb SET description = ? WHERE description_id = ? AND package = ? AND description <> ?", undef,
								$description, $description_id, $package, $description);
					}

					# Track changes in priority. Here we update the details of the description if one of:
					# - A package with this description comes along with a higher priority (could still be same package)
					# - The current package has a different (possibly lower) priority than before
					$dbh->do("UPDATE description_tb SET prioritize = ?, package = ?, source = ? WHERE description_id = ? AND CASE WHEN package = ? THEN prioritize <> ? ELSE prioritize < ? END", undef,
								$prioritize, $package, $source, $description_id, $package, $prioritize, $prioritize );
				} else {
					my $old_description_id=get_old_description_id($package);
					if ($old_description_id) {
						print "   changed description from $package ($source)\n" ;
                                                # search for translations of the old description:
	                                        # SELECT language FROM translation_tb where description_id=
						my $lang;
						my $sth = $dbh->prepare("SELECT language FROM translation_tb where description_id=?");
						$sth->execute($old_description_id);
						while(($lang) = $sth->fetchrow_array) {
							#print "       old description was translated in $lang\n" ;
							push @oldtranslang,$lang;
						}
					}
					# Can only happen if Packages file refer to md5 not in Translation-en file and we don't already know it.
					if( $descr_md5 ne get_md5_hex($description_orig) ) {
						die "  skipped package $package ($source), description doesn't match md5 ($descr_md5)";
					}
					$dbh->do("INSERT INTO description_tb (description_md5, description, package, source, prioritize) VALUES (?,?,?,?,?);", undef, $descr_md5,$description,$package,$source,$prioritize);
					$description_id=get_description_id_from_md5($descr_md5);
					$dbh->do("INSERT INTO description_tag_tb (description_id, tag, date_begin, date_end) VALUES (?,?,CURRENT_DATE,CURRENT_DATE);", undef, $description_id,$distribution);
					print "   add new description from $package ($source) with prio $prioritize\n" ;
				}
				$dbh->commit;   # commit the changes if we get this far
			};
			if ($@) {
				warn "Packages2db.pl: Transaction aborted because $@";
				$dbh->rollback; # undo the incomplete changes
			}
			if (($description_id)) {
				save_version_to_db($description_id,$version,$package,$source);
			}
			if (($description_id)) {
				save_milestone_to_db($description_id,$priority, $section, @task);
				save_tag_milestone_to_db($description_id,@tag);
				if ($#oldtranslang>=0) {
					save_oldtranslang_milestone_to_db($description_id,@oldtranslang);
				}
			}
			if (($description_id) and ($distribution eq 'sid')) {
				if (! is_description_id_active($description_id)) {
					save_active_to_db($description_id);

					my @parts = desc_to_parts($description);

					my @parts_md5;
					foreach (@parts) {
						push @parts_md5,get_md5_hex($_);
					}

					my $a=0;
					while ($a <= $#parts_md5 ) {
						&save_part_description_to_db($description_id,$parts_md5[$a]);
						$a++
					}
				}
			}


		}
		if (/^Package: ([\w.+-]+)/) { # new item
			$package=$1;
			$source=$1;
			$prioritize=40;
			$version="1";
			undef $descr_md5;
			$prioritize -= 1 if $package =~ /^(linux|kernel)-/i;
			$prioritize -= 1 if $package =~ /^(linux|kernel)-source/i;
			$prioritize -= 2 if $package =~ /^(linux|kernel)-patch/i;
			$prioritize -= 3 if $package =~ /^(linux|kernel)-header/i;
			$prioritize += 3 if $package =~ /^(linux|kernel)-image/i;
			$prioritize -= 5 if $package =~ /^python-/ && $package !~ /-doc$/;
			$prioritize -= 3 if $package =~ /lib/i;
			$prioritize -= 1 if $package =~ /-doc$/i;
			$prioritize -= 6 if $package =~ /-dev$/i;
			$prioritize -= 6 if $package =~ /-dbg$/i;
			$prioritize -= 6 if $package =~ /-dbgsym$/i;
		}
		if (/^Source: ([\w.+-]+)/) { # new item
			$source=$1;
		}
		if (/^Version: ([\w.+:~-]+)/) { # new item
			$version=$1;
		}
		if (/^Tag: (.+)/) { # new item
			@tag=split(',? +',$1);
			$tag=$1;
			$prioritize += 1;
			$prioritize += 2 if $tag =~ /role[^ ]+program/i;
			$prioritize += 1 if $tag =~ /role[^ ]+metapackage/i;
			$prioritize -= 2 if $tag =~ /role[^ ]+devel-lib/i;
			$prioritize -= 2 if $tag =~ /role[^ ]+source/i;
			$prioritize -= 1 if $tag =~ /role[^ ]+shared-lib/i;
			$prioritize -= 1 if $tag =~ /role[^ ]+data/i;
			# In newer packages files this can be split over multiple lines
			# This is a hack to prevent these lines being added to the description
			$in_descr = 0;
		}
		if (/^Priority: (\w+)/) { # new item
			$priority=$1;
			if ($priority  =~ /extra/i ) {
				$prioritize+=0;
			} elsif ($priority  =~ /optional/i ) {
				$prioritize+=5;
			} elsif ($priority  =~ /standard/i ) {
				$prioritize+=10;
			} elsif ($priority  =~ /important/i ) {
				$prioritize+=15;
			} elsif ($priority  =~ /required/i ) {
				$prioritize+=20;
			}
			if ($distribution  =~ /sid/i ) {
				$prioritize+=2;
			}
			if ($distribution  =~ /lliurex/i ) {
				$prioritize-=20;
			}
		}
		if (/^Maintainer: (.*)/) { # new item
		}
		if (/^Task: (.*)/) { # new item
			@task=split('[, ]+',$1);
			$prioritize+=2;
		}
		if (/^Section: (\w+)/) { # new item
			$section="$1";
			if ($section  =~ /devel/i ) {
				$prioritize-=3;
			} elsif ($section  =~ /net/i ) {
				$prioritize-=2;
			} elsif ($section  =~ /oldlibs/i ) {
				$prioritize-=3;
			} elsif ($section  =~ /libs/i ) {
				$prioritize-=3;
			}
		}
		if (/^Description: (.*)/) { # new item
			$description=$1 . "\n";
			# Following lines are part of this
			$in_descr = 1;
		}
		if (/^ / and $in_descr) {
		        # in_descr is because tag can also be multiple lines
			$description.=$_;
		}
		if (/^Description-md5: (.*)/) { # new item
		        my $md5 = $1;
		        $descr_md5 = $md5;
		        if (defined $description_en->{$md5}) {
		                $description = $description_en->{$md5}{'Description-en'} . "\n";
                        }
		}
	}
	close PACKAGES or die "packagefile failed";
}

sub parse_header_format
{
  my $fh = shift;
  my $sub = shift;

  my $lastfield = undef;
  my $hash;
  while(<$fh>)
  {
    chomp;
    if( /^([\w.-]+): (.*)/ )
    {
      $lastfield = $1;
      $hash->{$1} = $2;
    }
    elsif( /^( .*)/ )
    {
      $hash->{$lastfield} .= "\n$_";
    }
    elsif( /^$/ )
    {
      $sub->( $hash );
      $hash = {};
      $lastfield = undef;
    }
  }
}

# Loads the English translation file, indexed by md5
sub load_english_translations
{
        my $filename = shift;
        my $description_en = {};
        
        my $process_descr = sub {
                my $hash = shift;
                if( not exists $hash->{'Description-en'} ) {
                        die "Bad description $hash->{'Description-md5'}";
                }
                $description_en->{$hash->{'Description-md5'}} = $hash;
        };
	open my $fh, "<", "$filename" or die "open description_en file $filename failed";
	parse_header_format($fh, $process_descr);
	return $description_en;
}

my $description_en = {};
if ( $description_en_file ) {
        $description_en = load_english_translations($description_en_file);
}
        
if ( -r $packagefile ) {
	scan_packages_file($packagefile,$distribution,$description_en)
}

