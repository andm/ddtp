#!/bin/bash -e

. "${DDTP_CONFIG_FILE}"

INPORT2DB="./Packages2packages_tb.pl"
DB2FILE="./packages_tb2Packages.pl"

cd "${DDTP_BASE_DIR}"

DISTS="amd64 armel i386 mips64el mips mipsel armhf arm64 ppc64el s390x"
PART="main contrib"
DISTRIBUTION="${DDTP_DISTS_SUPPORTED} ${DDTP_DISTS_UNSTABLE} ${DDTP_DISTS_TESTING}"
MIRROR=http://ftp.de.debian.org/debian/dists

for distribution  in $DISTRIBUTION
do
	for part in $PART
	do
		# Download Translation-en file if present
		file="Packages/Translation-en_${distribution}_${part}"
		echo `date`: Translation-en ${distribution}/${part}

        [ -s $file.bz2 ] && mv $file.bz2 Packages/Translation-en.bz2
		[ -s $file.xz ] && mv $file.xz Packages/Translation-en.xz
		( wget -P Packages -q -m -nd ${MIRROR}/${distribution}/${part}/i18n/Translation-en.bz2 ||
		  wget -P Packages -q -m -nd ${MIRROR}/${distribution}/${part}/i18n/Translation-en.xz ) && {
			echo `date`: Translation-en file downloaded
		} || {
            echo `date`: Failed to download Translation-en ${distribution}/${part} 1>&2
        }
        [ -s Packages/Translation-en.bz2 ] && mv Packages/Translation-en.bz2 $file.bz2
        [ -s Packages/Translation-en.xz ] && mv Packages/Translation-en.xz $file.xz

		for arch in $DISTS
		do
			file="Packages/Packages_${distribution}_${part}_${arch}"

			echo `date`: ${distribution}/${part}/$arch
			[ -s $file.gz ] && mv $file.gz Packages/Packages.gz
			wget -P Packages -q -m -nd \
			    ${MIRROR}/${distribution}/${part}/binary-$arch/Packages.gz && {
				echo `date`: ${MIRROR}/${distribution}/${part}/binary-$arch/Packages.gz file downloaded
			} || {
				echo `date`: Failed to download ${distribution}/${part}/$arch "(${MIRROR}/${distribution}/${part}/binary-$arch/Packages.gz)" 1>&2
			}
			[ -s Packages/Packages.gz ] && mv Packages/Packages.gz $file.gz
		done
	done
done

for distribution  in $DISTRIBUTION
do
	for part in $PART
	do
		# Clear active before loading sid (which is what counts as active)
		echo -n "`date`: packages_tb "
		psql "${DDTP_PSQL_CONNECTION_STRING}" -c "TRUNCATE packages_tb"

		for arch in $DISTS
		do
			file="Packages/Packages_${distribution}_${part}_${arch}.gz"

			# Skip files older than 2 days...
			if [ -n "$(find "$file" -mtime +2)" ] ; then
				echo "skipped $file, too old"
				continue
			fi
			[ -f $file ] && echo -n `date`: $file
			[ -f $file ] && zcat $file | $INPORT2DB
			[ -f $file ] || echo no $file
		done

		file="Packages/Packages_${distribution}_${part}"
		echo -n "`date`: "
		$DB2FILE $file
		rm -f $file.bz2
		bzip2 $file
	done
done
# Regular vacuum to cut disk usage
echo -n "`date`: "
psql "${DDTP_PSQL_CONNECTION_STRING}" -c "VACUUM"
