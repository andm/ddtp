#!/bin/sh
#
# $Id$
#
# Copyright (C) 2008, Felipe Augusto van de Wiel <faw@funlabs.org>
# Copyright (C) 2008, Nicolas François <nicolas.francois@centraliens.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# On Debian systems, you can find the full text of the license in
# /usr/share/common-licenses/GPL-2

set -eu

BUILD_EXPECTED=0
for opt; do
	case "$opt" in
		"--build-expected")
			BUILD_EXPECTED=1
			;;
		*)
			echo "$0: Invalid option: $opt" >&2
			exit 1
			;;
	esac
done

TESTSUITE_DIR="./testsuite"
CHECK_SCRIPT="../checks/ddtp_i18n_check.sh"

if [ ! -d "$TESTSUITE_DIR" ]; then
	echo "The testsuite directory '$TESTSUITE_DIR' is not a directory" >&2
	exit 1
fi

find "$TESTSUITE_DIR" \( -name "*.gz" -o -name "*.bz2" -o -name "*.log" \) -delete

failures=""

prepare_testsuite () {
	pt_d="$1"
	pt_checker_options="$2"
	pt_basename=$(basename "$pt_d")
	pt_dirname=$(dirname "$pt_d")
	pt_tmp=$(mktemp -d testsuite-XXXXXX)
	cp -aH "$pt_d" "$pt_tmp/test"
	cp -aH "$pt_dirname.pkgs/$pt_basename" "$pt_tmp/test.pkgs"
	if [ "$BUILD_EXPECTED" = "0" ]; then
		if ! echo "$pt_checker_options" | grep -q -- "--debug"; then
			# Remove the lines which will only appear in case
			# of --debug.
			grep -v "^#DEBUG#" "$pt_d.expected" > "$pt_tmp/test.expected"
		else
			# Remove the #DEBUG# header.
			sed -e 's/^#DEBUG#//' "$pt_d.expected" > "$pt_tmp/test.expected"
		fi
	fi
	find "$pt_tmp" -name ".svn" -print0 | xargs -0 rm -rf

	echo "$pt_tmp"
}

clean_testsuite () {
	ct_tmp="$1"
	rm -rf "$ct_tmp"
}

run_test () {
	rt_options="$1"
	rt_tmp="$2"
	rt_expected_status="$3"
	rt_test_name="$4"
	rt_status=0

	$CHECK_SCRIPT $rt_options "$rt_tmp/test" "$rt_tmp/test.pkgs" > "$rt_tmp/test.log" 2>&1 || {
		rt_status=$?
	}
	echo "status: $rt_status" >> "$rt_tmp/test.log"

	# Remove the dates in the diff headers
	# Remove the date in the success message
	# Remove the random part from the checker's tmp dir
	# Remove the random part from the testsuite's tmp dir
	sed -i -e 's/^\(\(---\|+++\) .*\)\t.*$/\1/'                                   \
	       -e 's/\(structure validated successfully (\).*)$/\1\$SUCCESS_DATE\$)/' \
	       -e 's/ddtp_dinstall_tmpdir\......./ddtp_dinstall_tmpdir.XXXXXX/'       \
	       -e "s|$rt_tmp|testsuite-XXXXXX|"                                       \
	       "$rt_tmp/test.log"

	if [ "$BUILD_EXPECTED" = "0" ]; then
		cp "$rt_tmp/test.log" "$rt_test_name.log"
		cmp --quiet "$rt_tmp/test.expected" "$rt_tmp/test.log" || return 1
		rm -f "$rt_tmp/test.log"
	else
		if [ "$rt_status" != "$rt_expected_status" ]; then
			return 1
		fi
	fi
}

for checker_options in "" "--dry-run" "--debug" "--dry-run --debug"; do
	echo ""
	echo "Testing options '$checker_options'"
for d in $TESTSUITE_DIR/{good,bad}/*; do
	[ ! -d "$d" ] && continue
	tmp=$(prepare_testsuite "$d" "$checker_options")

	case $(dirname "$d") in
		"$TESTSUITE_DIR/good")	expected_status=0;;
		"$TESTSUITE_DIR/bad")	expected_status=1;;
	esac

	if run_test "$checker_options" "$tmp" $expected_status "$d"; then
		echo " OK* $d"
	else
		echo "*NO  $d"
		failures="$failures $d.log (options: '$checker_options')"
	fi

	if [ "$BUILD_EXPECTED" = "1" ]; then
		cp "$tmp/test.log" "$d.expected"
	fi

	clean_testsuite "$tmp"
done
done

echo ""

if [ -n "$failures" ]; then
	echo "The following testsuite failed: $failures"
else
	echo "No testsuites failed"
fi

