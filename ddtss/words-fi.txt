logging	lokikirjaus
daemon	taustaprosessi
multi-threaded	monisäikeinen
virtual	näennäis-
transitional	siirtymä-
dummy	tyhjä, vale
user space	ytimen ulkopuolinen (muistialue)
userspace	ytimen ulkopuolinen (muistialue)
user-space	ytimen ulkopuolinen (muistialue)
kernel space	ytimelle varattu muistialue
kernelspace	ytimelle varattu muistialue
kernel-space	ytimelle varattu muistialue
userland	ytimen ulkopuolinen (ytimen ulkopuolisessa muistialueessa toimiva ohjelma tai skripti)
URL	Internet-osoite
plug-in	liitännäinen, lisämoduuli, lisäosa
pluggable	liitettävä
authentication	todennus
block device	lohkolaite
subset	osajoukko
interpreter	tulkki
mail transport agent	sähköpostin välityspalvelin
upstream	alkuperäis-, alkuperäinen tekijä
setting of	säätää
key binding	näppäinyhdistelmä
keybinding	näppäinyhdistelmä
microcontroller	mikro-ohjain
align	rinnastaa, linjata
unrooted tree	juureton puu (puukaavio jossa kaavion suunnalla ja osien keskinäisellä sijoittelulla ei ole merkitystä)
modkey	muuntonäppäin
multichar	monimerkki
feature extraction	piirteenirrotus
keymap	näppäinkaavio, näppäinjärjestys
keyboard layout	näppäinkaavio, näppäinjärjestys
