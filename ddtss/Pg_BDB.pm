use lib '/srv/ddtp.debian.org/ddts/bin/';
use strict;

package Pg_BDB;

use ddts_common;
use DBI;
use DBD::Pg qw(:pg_types);
use Encode;
#DBI->trace(1);
my @DSN = ("DBI:Pg:service=ddtp", "", "");

my ($_dbh, $_inuse);
sub _get_handle()
{
    # Only open connection once
    if (not defined $_dbh)
    {
        $_dbh = DBI->connect(@DSN,
            { PrintError => 0,
              RaiseError => 1,
              AutoCommit => 1,
            });

        die $DBI::errstr unless $_dbh;
    }
    
    die "Reuse of DB" if $_inuse;
    
    $_inuse = 1;
    return $_dbh;
}

sub _release_handle()
{
    die "Release of DB without use" unless $_inuse;
    $_inuse = 0;
}
                    
sub open_read()
{
    my $self = bless { mode => "r", dbh => _get_handle() }, 'Pg_BDB';
 
    my $dbh = $self->{dbh};   
    $dbh->begin_work or die $dbh->errstr;
    $dbh->do("LOCK TABLE ddtss IN ACCESS SHARE MODE") or die $dbh->errstr;
    
    return $self;
}

sub open_write()
{
    my $self = bless { mode => "w", dbh => _get_handle() }, 'Pg_BDB';
    
    my $dbh = $self->{dbh};   
    $dbh->begin_work or die $dbh->errstr;
    $dbh->do("LOCK TABLE ddtss IN ACCESS EXCLUSIVE MODE") or die $dbh->errstr;
    
    return $self;
}

sub close()
{
    my $self = shift;
    _release_handle();
    my $dbh = $self->{dbh};
    $self->{dbh} = undef;
    $dbh->commit;
}

# These methods emulate the berkeley DB interface
sub get($$)
{
    my $self = shift;
    my $key = shift;
    my $value = undef;
    my $res;
    
    my $dbh = $self->{dbh};
    
    my $sth = $dbh->prepare("SELECT value FROM ddtss WHERE key = ?", {});
    $sth->execute($key) or die $dbh->errstr;
    if ($sth->rows)
    {
        ($value) = $sth->fetchrow_array;
        $res = 0;
    }
    else
    {
        $res = 1;
    }

    $_[0] = $value;
#    print STDERR "get($key) => [".(defined($value)?$value:'(undef)')."]\n";
    
    return $res;
}

sub put($$)
{
    my $self = shift;
    my $key = shift;
    my $value = shift;

    if (Encode::is_utf8($value)) {
        warn "put($key) non-utf8 value|";
	#warn "put($key) non-utf8 value: $value|";
    }
    $value=get_utf8($value);
    my $dbh = $self->{dbh};
    
    if( $self->{mode} eq "r" )
    {
        warn "put($key) in readonly transaction";
        return 1;
    }
    
    my $sth = $dbh->prepare("UPDATE ddtss SET value = ? WHERE key = ?", {});
    $sth->execute($value, $key) or die $dbh->errstr;
    
    if ($sth->rows == 0 )  # No rows updated, needs insert
    {
        $sth = $dbh->prepare("INSERT INTO ddtss (key, value) VALUES (?,?)", {});
        $sth->execute($key, $value) or die $dbh->errstr;
    }
    return undef;
}

sub del($)
{
    my $self = shift;
    my $key = shift;
    
    my $dbh = $self->{dbh};
    
    if( $self->{mode} eq "r" )
    {
        warn "del($key) in readonly transaction";
        return 1;
    }
    
    my $sth = $dbh->prepare("DELETE FROM ddtss WHERE key = ?", {});
    $sth->execute($key) or die $dbh->errstr;

    return undef;
}

# And this is the search interface, to be used by the ddtss_match method
sub search($&;$)
{
    my $self = shift;
    my $prefix = shift;
    my $callback = shift;
    my $regex = shift;   # Regex is optional, for performance on the db side really

    my $sth;
    my $dbh = $self->{dbh};
        
    warn "prefix: $prefix\n";
    warn "regex: $regex\n";
    if( defined $regex )
    {
        $sth = $dbh->prepare("SELECT key, value FROM ddtss WHERE key LIKE ? AND key ~ ? ORDER BY key", {});
        $sth->execute($prefix."%", $regex);
    }
    else
    {
        $sth = $dbh->prepare("SELECT key, value FROM ddtss WHERE key LIKE ? ORDER BY key", {});
        $sth->execute($prefix."%");
    }
    while ( my $row = $sth->fetchrow_arrayref )
    {
        $callback->( $row->[0], $row->[1] );
    }
    return;
}

# Get statistics for a language
sub get_stats($$)
{
    my $self = shift;
    my $lang = shift;
    my $dbh = $self->{dbh};

    my $pending = 0;
    my $review = 0;
    my $sent = 0;

    my $sth = $dbh->prepare("SELECT COUNT(1) FROM ddtss WHERE key LIKE ? ", {});
    $sth->execute($lang."/done/%") or die $dbh->errstr;
    if ($sth->rows)
    {
        $sent= $sth->fetchrow_array;
    }


    my $sth = $dbh->prepare("SELECT COUNT(1) FROM ddtss WHERE key LIKE ? AND value LIKE 'untranslated%'", {});
    $sth->execute($lang."/packages/%") or die $dbh->errstr;
    if ($sth->rows)
    {
        $pending = $sth->fetchrow_array;
    }

    my $sth = $dbh->prepare("SELECT COUNT(1) FROM ddtss WHERE key LIKE ? AND key NOT LIKE ? AND value NOT LIKE'untranslated%'", {});
    $sth->execute($lang."/packages/%", $lang."/packages/%/%") or die $dbh->errstr;
    if ($sth->rows)
    {
        $review= $sth->fetchrow_array;
    }

    my %res;
    $res{untranslated} = $pending;
    $res{done} = $sent;
    $res{forreview} = $review;
    return \%res;
}
-1;
