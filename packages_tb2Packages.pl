#!/usr/bin/perl -w
use strict;
use DBI;
use open ':encoding(UTF-8)';
my @DSN = ("DBI:Pg:service=ddtp", "", "");

my $dbh = DBI->connect(@DSN,
    { PrintError => 0,
      RaiseError => 1,
      AutoCommit => 0,
    });

die $DBI::errstr unless $dbh;

my $data = "/srv/ddtp.debian.org/Packages/";

my $file = shift;
die "Usage: $0 <Packages>\n" unless defined $file;

export_packages();      # Read packages file
exit;

sub export_packages
{
  my ($package,$source,$version,$tag,$priority,$maintainer,$task,$section,$description,$description_md5);

  print " Export package file \n";
  my $fh;
  open $fh, '>', "$file" or die "Couldn't write to $file ($!)\n";

  my $sth = $dbh->prepare("SELECT package,source,version,tag,priority,maintainer,task,section,description,description_md5 FROM packages_tb ORDER BY package");
  $sth->execute;
  while ( ($package,$source,$version,$tag,$priority,$maintainer,$task,$section,$description,$description_md5) = $sth->fetchrow_array ) {
    print $fh "Package: $package\n";
    print $fh "Source: $source\n";
    print $fh "Version: $version\n";
    print $fh "Tag: $tag\n" if $tag;
    print $fh "Priority: $priority\n";
    print $fh "Maintainer: $maintainer\n";
    print $fh "Task: $task\n" if $task;
    print $fh "Section: $section\n";
    print $fh "Description: $description\n";
    print $fh "Description-md5: $description_md5\n" if length($description_md5) == 32;
    print $fh "\n";
  }
  close $fh;
}

