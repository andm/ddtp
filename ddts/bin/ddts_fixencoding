#!/usr/bin/perl

use strict;
use warnings;

use DBI;
use Encode;
my @DSN = ("DBI:Pg:service=ddtp", "", "");

my $dbh = DBI->connect(@DSN,
    { PrintError => 0,
      RaiseError => 1,
      AutoCommit => 0,
    });

die $DBI::errstr unless $dbh;

my $really_do = 0;

if( $#ARGV >= 0 and $ARGV[0] eq "-f" )
{ $really_do = 1 }

# Fix translation table
print STDERR "Fixing translations...\n";
my $rv = $dbh->prepare( "select translation_id, translation, language, description_id from translation_tb" );

$rv->execute();

my @row;

while ( @row = $rv->fetchrow_array ) {
  my $str = check_encoding( $row[1], $row[2] );
  if( not defined $str )
  {
    print STDERR "Unable to fix translation (id=$row[0], lang=$row[2], desc_id=$row[3]), deleting\n";
    
    $really_do && $dbh->do( "DELETE FROM translation_tb WHERE translation_id = ?", undef, $row[0] );
  }
  elsif( $str ne $row[1] )
  {
    print "Fixing translation: (id=$row[0], lang=$row[2], desc_id=$row[3])\n";
    
    $really_do && $dbh->do( "UPDATE translation_tb SET translation = ? WHERE translation_id = ?", undef, $str, $row[0] );
  }
}

$dbh->commit;

# Fix parts table
print STDERR "Fixing parts...\n";
$rv = $dbh->prepare( "select part_id, part, language from part_tb" );

$rv->execute();

while ( @row = $rv->fetchrow_array ) {
  my $str = check_encoding( $row[1], $row[2] );
  if( not defined $str )
  {
    print STDERR "Unable to fix part (id=$row[0], lang=$row[2]), deleting\n";
    
    $really_do && $dbh->do( "DELETE FROM part_tb WHERE part_id = ?", undef, $row[0] );
  }
  elsif( $str ne $row[1] )
  {
    print "Fixing part: (id=$row[0], lang=$row[2])\n";
    
    $really_do && $dbh->do( "UPDATE part_tb SET part = ? WHERE part_id = ?", undef, $str, $row[0] );
  }
}

$dbh->commit;

$dbh->disconnect;
exit;

sub check_encoding
{
  my($str, $lang) = @_;

  eval {
    # This will exit the eval if the code isn't valid UTF-8
    Encode::decode_utf8( $str, Encode::FB_CROAK | Encode::LEAVE_SRC);
  };
  # Valid UTF-8, next
  return $str unless $@;

  eval {
    my $charset = "";
    if( $row[2] eq "uk" ) { $charset = "KOI8-R" }
    if( $row[2] eq "ja" ) { $charset = "EUC-JP" }
    if( $row[2] eq "it" ) { $charset = "ISO-8859-1" }
    if( $row[2] eq "zh_TW" ) { $charset = "BIG5" }
    if( $row[2] eq "zh_CN" ) { $charset = "gb2312" }

    if( $charset eq "" ) { print STDERR "Unexpected language (lang=$lang id=$row[0])\n"; return $str; }

    Encode::from_to( $str, $charset, "UTF-8", Encode::FB_CROAK );
    
    # Recode worked, return new string
  };
  return $str unless $@;

  print STDERR "Recode failure: lang=$lang, str=[$str] ($@)\n";
  
  return undef;
}
