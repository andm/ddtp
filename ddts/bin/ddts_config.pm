package ddts_config;

use strict;

use vars qw(@ISA @EXPORT @EXPORT_OK %EXPORT_TAGS $VERSION
	$basedir
	$tmpdir
	$bts_html_base
	$update_html_base
	$p_update_html_base
	$lp_status_html_base
	$maintainer_html_base
	$bindir
	$logdir
	$statusdir
	$guidesdir
	$hotnewsdir
	$mailtextdir
	$my_tmpdir
	$server_from_address
	$server_admin_address
	$server_log_address
	%coordinator_address
	%log_address
	%charset
	$uni_sec
	);

use Exporter;
$VERSION=1.00;
@ISA=qw(Exporter);

@EXPORT =qw(
	$basedir
	$tmpdir
	$bts_html_base
	$update_html_base
	$p_update_html_base
	$lp_status_html_base
	$maintainer_html_base
	$bindir
	$logdir
	$statusdir
	$guidesdir
	$mailtextdir
	$hotnewsdir
	$my_tmpdir
	$server_from_address
	$server_admin_address
	$server_log_address
	%coordinator_address
	%log_address
	%charset
	$uni_sec
	);


$basedir="/srv/ddtp.debian.org/ddts";
$bts_html_base="$basedir/bts/";
$update_html_base="$basedir/update_html/";
$p_update_html_base="$basedir/old_desc/";
$lp_status_html_base="$basedir/lp_status/";
$maintainer_html_base="$basedir/maintainer/";
$tmpdir="$basedir/tmp/";
$bindir="$basedir/bin/";
$logdir="$basedir/log/";
$statusdir="$basedir/status/";
$guidesdir="$basedir/ddts-text/guides";
$hotnewsdir="$basedir/ddts-text/news/";
$mailtextdir="/srv/ddtp.debian.org/ddtp-text/mails/";
$my_tmpdir="$tmpdir/$$";

$server_from_address="pdesc\@ddtp.debian.org";
$server_admin_address="grisu\@debian.org";
$server_log_address="logmail\@ddtp.debian.org";

$coordinator_address{'de'}="grisu\@debian.org";
$coordinator_address{'da'}="devnull\@ddtp.debian.org";
$coordinator_address{'fr'}="devnull\@ddtp.debian.org";
$coordinator_address{'it'}="devnull\@ddtp.debian.org";
$coordinator_address{'nl'}="devnull\@ddtp.debian.org";
$coordinator_address{'pl'}="devnull\@ddtp.debian.org";
$coordinator_address{'hu'}="devnull\@ddtp.debian.org";
$coordinator_address{'pt_AO'}="devnull\@ddtp.debian.org";
$coordinator_address{'pt_BR'}="devnull\@ddtp.debian.org";
$coordinator_address{'pt_PT'}="devnull\@ddtp.debian.org";
$coordinator_address{'sk'}="devnull\@ddtp.debian.org";
$coordinator_address{'sv_SE'}="devnull\@ddtp.debian.org";
$coordinator_address{'ja'}="devnull\@ddtp.debian.org";
$coordinator_address{'uk'}="devnull\@ddtp.debian.org";
$coordinator_address{'ru'}="devnull\@ddtp.debian.org";
$coordinator_address{'cs'}="devnull\@ddtp.debian.org";
$coordinator_address{'fi'}="devnull\@ddtp.debian.org";
$coordinator_address{'es'}="devnull\@ddtp.debian.org";
$coordinator_address{'eo'}="devnull\@ddtp.debian.org";
$coordinator_address{'he'}="devnull\@ddtp.debian.org";

$log_address{'de'}="devnull\@ddtp.debian.org";
$log_address{'da'}="devnull\@ddtp.debian.org";
$log_address{'fr'}="devnull\@ddtp.debian.org";
$log_address{'it'}="devnull\@ddtp.debian.org";
$log_address{'it'}="devnull\@ddtp.debian.org";
$log_address{'nl'}="devnull\@ddtp.debian.org";
$log_address{'pl'}="devnull\@ddtp.debian.org";
$log_address{'hu'}="devnull\@ddtp.debian.org";
$log_address{'pt_AO'}="devnull\@ddtp.debian.org";
$log_address{'pt_BR'}="devnull\@ddtp.debian.org";
$log_address{'pt_PT'}="devnull\@ddtp.debian.org";
$log_address{'sk'}="devnull\@ddtp.debian.org";
$log_address{'sv_SE'}="devnull\@ddtp.debian.org";
$log_address{'ja'}="devnull\@ddtp.debian.org";
$log_address{'uk'}="devnull\@ddtp.debian.org";
$log_address{'ru'}="devnull\@ddtp.debian.org";
$log_address{'cs'}="devnull\@ddtp.debian.org";
$log_address{'fi'}="devnull\@ddtp.debian.org";
$log_address{'es'}="devnull\@ddtp.debian.org";
$log_address{'eo'}="devnull\@ddtp.debian.org";
$log_address{'he'}="devnull\@ddtp.debian.org";

$charset{'de'}="ISO-8859-1";
$charset{'da'}="ISO-8859-1";
$charset{'fr'}="ISO-8859-1";
$charset{'it'}="ISO-8859-1";
$charset{'nl'}="ISO-8859-1";
$charset{'pl'}="ISO-8859-2";
$charset{'hu'}="ISO-8859-2";
$charset{'pt_AO'}="ISO-8859-1";
$charset{'pt_BR'}="ISO-8859-1";
$charset{'pt_PT'}="ISO-8859-1";
$charset{'sk'}="ISO-8859-2";
$charset{'sv_SE'}="ISO-8859-1";
$charset{'ja'}="euc-jp";
$charset{'uk'}="koi8-u";
$charset{'ru'}="koi8-r";
$charset{'cs'}="ISO-8859-2";
$charset{'fi'}="ISO-8859-1";
$charset{'es'}="ISO-8859-1";
$charset{'eo'}="UTF-8";
$charset{'he'}="ISO-8859-8";

$uni_sec=time();

1;
