#!/bin/bash

. "${DDTP_CONFIG_FILE}"

cd "${DDTP_BASE_DIR}"

LOGDIR=/srv/ddtp.debian.org/cronlog
LOGPREFIX=$LOGDIR/update.cron.$(date "+%Y%m%d-%H%M")

[ ! -d "$LOGDIR" ] && mkdir "$LOGDIR"

date                                      >> $LOGPREFIX.log
#./Packages2packages_tb.sh                 >> $LOGPREFIX.log 2>> $LOGPREFIX.err
#./Packages2db.sh                          >> $LOGPREFIX.log 2>> $LOGPREFIX.err
#./completeTranslations.sh                 >> $LOGPREFIX.log 2>> $LOGPREFIX.err
./db2web.sh                               >> $LOGPREFIX.log 2>> $LOGPREFIX.err
#./file2Translation.sh                     >> $LOGPREFIX.log 2>> $LOGPREFIX.err
./file2Translation_udd.sh                 >> $LOGPREFIX.log 2>> $LOGPREFIX.err
./db2po.sh zh_CN sid                      >> $LOGPREFIX.log 2>> $LOGPREFIX.err

# Regenerate the stats files
# Regenerate the stats files
DISTS="${DDTP_DISTS_UNSTABLE} ${DDTP_DISTS_SUPPORTED}"
for distribution in $DISTS
do
    ./ddts-stats $distribution            >> $LOGPREFIX.log 2>> $LOGPREFIX.err
done

#cp -a /home/grisu/public_html/ddtp/* /var/www/ddtp/

#/usr/sbin/logrotate --state /srv/ddtp.debian.org/lib/logrotate.state /srv/ddtp.debian.org/logrotate.config

cat $LOGPREFIX.err

